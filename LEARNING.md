# Ting de skal gjøre

## Basiskomponenter
* Implementere container
* Implementere sortert innsetting av items i GameMap (hvis største item er først, tegne bare første item)
* Implementere kart – kanskje det er noe i Grid / MultiGrid de kan gjøre noe med?
* Implementere kartgenerator

## Spillting
* Implementere en IPlayer
* Implementere 1–3 IMonster
* Implementere 1–3 IItem

# Ting de skal lære / bruke
## Nye ting:

* Småobjekter (f.eks. location) som er “immutable” – de kan ikke endres men returnerer heller nye objekter.
* Være mer forsiktig med forkrav; sjekke parametere.
* Factories
* (Litt) Events


#### Optional

* Streams. Mulig å bruke dette sammen med grid.
* Lambda. Kan brukes til å fylle grid.

## Ting vi bør forklare:

#### Kontrollflyt-/kjøringsmodeller
De har sett forskjellige varianter av dette allerede, men de bør lære eksplisitt hva som foregår. E.g., "kan gjøre rede for..."

TODO: Skriv kort fornuftig forklaring på dette.

* `main`-styrt program:
   * Leser inndata og parametre, utfører beregning, skriver resultat
   * Evt.: main-loop som leser input fra bruker, utfører kommandoer, skriver resultat
* event-styrt program:
   * Oppstart og innlesing
   * Metoder i programmet vårt blir kalt når noe skjer
      * Tidsstyrt (som sett i labbene og på forelesning)
      * Styrt av andre hendelser, som mus/tastatur (evt. kombinert med tidssignal)
      * Nettverkstjenester funker slik også – en nettverksforespørsel er en "hendelse"
      * Hendelsene er gjerne representert med Event eller Request objekter, som inneholder nødvendig informasjon (tilsvarende en innlest kommando eller et parameter til et program)
    * main-loopen ligger gjerne i et eller annet rammeverk, og gjør noe slikt som `while(true) { waitForSomethingToHappen(); for(Something s : thingsThatHappened) { findHandlerFor(s).handle(new Event(s)); } }` (i prinsippet tilsvarende til en evig løkke som venter på `Scanner` – bare at vi gjerne kan vente på flere kilder samtidig)
   * Vanligvis brukt til interaktive programmer og nettverkstjenester
* (evt: program som matematisk funksjon: *f: input -> output* eller *f: input, state -> output, state'* (React))
* Andre ting som er verd å nevne?

## Såvidt gått gjennom på forelesning:

* Iteratorer og hvordan de lages
    * Kan evt. bruke en ferdig løsning
    
## Kjente ting

### Ikke snakket så mye om ennå

* Exceptions. Kjent fra INF100.
* Bygge abstraksjoner oppå hverandre. Kan utledes hva hva de har sett (f.eks. MyGrid bygger på IList), men ikke snakket så mye om i praksis.
