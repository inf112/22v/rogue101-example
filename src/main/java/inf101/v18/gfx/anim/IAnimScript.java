package inf101.v18.gfx.anim;

import java.util.function.Consumer;

import inf101.v18.gfx.gfxmode.Point;
import javafx.animation.Interpolator;

public interface IAnimScript {
	void play();

	void stop();

	void pause();

	IAnimScript then(IAnimScript next);
	
	IAnimScript then();
	
	IAnimScript transition(int millis, Consumer<Double> step);
	
	IAnimScript after(int millis);
	

	/**
	 * Set interpolation strategy for the start of the animation.
	 * <p>
	 * The interpolator controls time acceleration/deceleration. Default is LINEAR.
	 * 
	 * @param startInterpolation
	 *            Acceleration at start of animation
	 * @return this
	 */
	IAnimScript setStartInterpolation(Interpolator startInterpolation);

	/**
	 * Set interpolation strategy for the when this animation is preceded or succeed
	 * by another.
	 * <p>
	 * The interpolator controls time acceleration/deceleration. Default is LINEAR.
	 * <p>
	 * The midpoint interpolation is only used when animations are combined in a
	 * {@link #chain(IAnimScript)}. If null, the normal start or end
	 * interpolation is used instead.
	 * 
	 * @param startInterpolation
	 *            Acceleration at start of animation
	 * @return this
	 */
	IAnimScript setMidInterpolation(Interpolator midInterpolation);

	/**
	 * Set interpolation strategy for the when this animation is preceded or succeed
	 * by another.
	 * <p>
	 * The interpolator controls time acceleration/deceleration. Default is LINEAR.
	 * <p>
	 * The midpoint interpolation is only used when animations are combined in a
	 * {@link #chain(IAnimScript)}. If null, the normal start or end
	 * interpolation is used instead.
	 * 
	 * @param startInterpolation
	 *            Acceleration at start of animation
	 * @return this
	 */
	IAnimScript setEndInterpolation(Interpolator endInterpolation);
}
