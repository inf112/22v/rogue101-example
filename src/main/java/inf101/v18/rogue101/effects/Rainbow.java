package inf101.v18.rogue101.effects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import inf101.v18.gfx.gfxmode.Direction;
import inf101.v18.gfx.gfxmode.ITurtle;
import inf101.v18.gfx.gfxmode.Point;
import inf101.v18.gfx.gfxmode.TurtlePainter;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.ActionEvent;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.BlendMode;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Paint;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.StrokeLineCap;
import javafx.util.Duration;

public class Rainbow {
	private Timeline particleTimeline = null;

	public static final boolean RAINBOW_POLYS = false;
	public static final boolean DEBUG_POLYS = false;
	public static final List<Color> RAINBOW_COLOURS = Arrays.asList(Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN,
			Color.BLUE, Color.INDIGO, Color.DARKVIOLET);
	private List<Point> particlePos = new ArrayList<>();
	private List<Point> particleVelocity = new ArrayList<>();
	private List<Paint> particlePaint = new ArrayList<>();
	private List<Double> particleRotation = new ArrayList<>();
	private List<Double> particleSize = new ArrayList<>();
	private List<Integer> particleLife = new ArrayList<>();
	private Random random = new Random();
	private double x;
	private double y;
	private int count = 0;
	private double startAngle = 0;

	private ITurtle painter;

	private int arcTime;

	private int totalTime;
	private double rainbowWidth = 1;
	private double radius;
	private double flipX = 1;
	private double flipY = 1;
	private double lifeAdjust = 1000;
	private Timeline drawTl;

	private double degrees;
	public static final Stop[] RAINBOW_STOPS = new Stop[RAINBOW_COLOURS.size()];
	static {
		for (int i = 0; i < RAINBOW_COLOURS.size(); i++) {
			RAINBOW_STOPS[i] = new Stop(((double) i / RAINBOW_COLOURS.size()), //
					Color.hsb(360.0 * i / RAINBOW_COLOURS.size(), 1.0, 1.0)); // RAINBOW_COLOURS.get(i));
		}
	}

	public Rainbow(ITurtle painter, double radius, int totalTime, int arcTime) {
		System.out.println("new rainbow!");
		this.painter = painter;
		this.x = painter.getPos().getX();
		this.y = painter.getPos().getY();
		this.totalTime = totalTime;
		this.arcTime = arcTime;
		this.radius = radius;
	}

	public void play(Point pos, double degrees) {
		x = pos.getX();
		y = pos.getY();
		this.degrees = degrees;
		flipY = (degrees == 180 || degrees == 90) ? -1 : 1;
//		flipY = (degrees == 90) ? -1 : 1;
		if (degrees == 90 || degrees == 270) {
			rainbowWidth = 0.5;
		} else {
			rainbowWidth = 1.0;
		}
		count++;
		radius = 16;// * count;

		if (particleTimeline == null)
			makeParticleJob();
		if (drawTl != null) {
			if (drawTl.getStatus() == Animation.Status.RUNNING)
				draw(1.0);
			startAngle = 0;
			drawTl.playFromStart();
			return;
		}
		DoubleProperty fraction = new SimpleDoubleProperty(0);

		fraction.addListener((frac, oldVal, newVal) -> {
			draw(newVal.doubleValue());
		});

		final KeyValue kv1 = new KeyValue(fraction, 0.0, Interpolator.LINEAR);
		final KeyValue kv2 = new KeyValue(fraction, 1.0, Interpolator.LINEAR);

		final KeyFrame kf1 = new KeyFrame(Duration.millis(0), kv1);
		final KeyFrame kf2 = new KeyFrame(Duration.millis(arcTime), kv2);

		drawTl = new Timeline(kf1, kf2);
		drawTl.setCycleCount(1);
		drawTl.playFromStart();
	}

	private double getBandWidth() {
		return radius * rainbowWidth / RAINBOW_COLOURS.size();
	}

	public void draw(double fraction) {
		System.out.println("new draw: " + fraction);
		double r = radius;

		painter.save();
		painter.jumpTo(x, y);
		painter.turnTo(degrees);
		painter.jump(radius);

		int start = particlePos.size();
		double angle = startAngle;
		for (Color col : RAINBOW_COLOURS) {
			// bandWidth = radius * width / RAINBOW_COLOURS.size();
			// painter.turn(90);

			// painter.setPenSize(getBandWidth());
			// painter.setInk(new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, new
			// Stop(0, col),
			// new Stop(1, col.deriveColor(0.0, 1.0, 1.0, 1.0))));
			
			startAngle = drawStarTrack(painter, col.deriveColor(0.0, 1.0, 1.0, 1.0), r, angle, fraction,
					getBandWidth());

//			 startAngle = drawStarArc(painter, col.deriveColor(0.0, 1.0, 1.0, 1.0), r,
//			 angle, 180 * fraction,
//			 getBandWidth());

			// if(fraction > 0.5)
			// arcCut(painter, col, bandWidth, radius, 360 * (Math.max(0.4, fraction) -
			// 0.4));
			// painter.setInk(Color.BLACK);
			// painter.setPenSize(bandWidth / 3);
			// drawArc(painter, col, numSteps, radius, 360);
			// if (fraction * 100 % 8 == i) {
			// }

			// painter.turn(-90);
			// painter.jump(getBandWidth());
			r -= getBandWidth();
		}

		if (stepParticles(start, 0) > 0 && particleTimeline.getStatus() != Animation.Status.RUNNING) {
			particleTimeline.playFromStart();
		}

		// painter.as(GraphicsContext.class).restore();
		painter.restore();
		System.out.println("end draw: " + fraction);
	}

	private double drawStarArc(ITurtle painter, Color col, double radius, double startAngle, double endAngle,
			double width) {
		double angle = startAngle;
		double step = 3;
		while (angle < endAngle) {
			painter.save();
			double a = Math.toRadians(
					180 - painter.getAngle() + angle + Math.min(endAngle - angle, step) * random.nextDouble());
			painter.jump(new Point(flipX * Math.cos(a) * radius, flipY * Math.sin(a) * radius));
			painter.turn(-angle);
			painter.turn(90);
			painter.jump(random.nextGaussian() * width);
			painter.turn(-90);
			if (random.nextInt(2) == 0)
				painter.turn(180);
			particlePos.add(painter.getPos());
			particleVelocity.add(new Direction(painter.getAngle()).getMovement(width / 4 * random.nextGaussian()));
			particlePaint.add(col);
			particleRotation.add(painter.getAngle());
			particleSize.add((0.5 + random.nextDouble()) * width);
			particleLife
					.add((int) ((8 * totalTime + totalTime * random.nextGaussian()) / 8 + lifeAdjust * angle / 180.0));

			painter.restore();
			angle += step;
		}
		return angle;
	}

	private double drawStarTrack(ITurtle painter, Color col, double length, double startFrac, double endFrac,
			double width) {
		double frac = startFrac;
		double step = 10.0 / 180.0;
		painter.save();
		painter.jump(-radius);
		painter.turn(flipY * 90);
		painter.jump(length - (radius * (1 - rainbowWidth)));
		painter.turn(-flipY * 90);
		painter.jump(frac * radius);
		while (frac < endFrac) {
			painter.save();
			painter.turn(90);
			painter.jump(random.nextGaussian() * width);
			painter.turn(-90);
			// if (random.nextInt(2) == 0)
			// painter.turn(180);
			particlePos.add(painter.getPos());
			particleVelocity
					.add(new Direction(painter.getAngle()).getMovement(width / 4 * -Math.abs(random.nextGaussian())));
			particlePaint.add(col);
			particleRotation.add(painter.getAngle());
			particleSize.add((0.5 + random.nextDouble()) * width);
			particleLife.add((int) ((8 * totalTime + totalTime * random.nextGaussian()) / 8 + lifeAdjust * frac));

			painter.restore();
			painter.jump(step * radius);
			frac += step;
		}
		painter.restore();
		return frac;
	}

	private void drawArc(ITurtle painter, Color col, int stepsPerCircle, double radius, double arcStart, double arcEnd,
			boolean star) {
		arcStart += 5;
		double circumference = 2 * Math.PI * (radius);
		double arcLengthInitial = circumference * (arcEnd - arcStart) / 360.0;
		double arcLength = arcLengthInitial;
		double stepLength = circumference / stepsPerCircle;
		double arcStep = 360.0 / stepsPerCircle;
		painter.save();
		double a = Math.toRadians(arcStart);
		painter.jumpTo(painter.getPos().getX() + (1 - Math.cos(a)) * radius,
				painter.getPos().getY() - Math.sin(a) * radius);

		painter.turn(-arcStart);

		// painter.as(TurtlePainter.class).beginPath();
		// painter.as(GraphicsContext.class).setLineCap(StrokeLineCap.BUTT);
		double l = Math.min(arcLength, 0.5 * stepLength);
		double opacity = 5;
		while (arcLength > 0) {
			// painter.setInk(col.deriveColor(0.0, 1.0, 1.0, (opacity++)/10));
			painter.save();
			painter.as(TurtlePainter.class).beginPath();
			painter.setInk(col);
			traceStar(painter, 5, 36, 10 + (10) * random.nextDouble());
			painter.as(TurtlePainter.class).fillPath();
			painter.restore();
			painter.jump(l);
			painter.turn(-arcStep);
			arcLength -= l;
			l = Math.min(arcLength, stepLength);
		}
		// painter.as(TurtlePainter.class).endPath();

		if (star && arcStart == -1) {
			particlePos.add(painter.getPos());
			particleVelocity.add(new Direction(90 + 45 * random.nextGaussian()).getMovement(10));
			particlePaint.add(col);
			particleRotation.add(360.0 * random.nextDouble());
		}
		painter.restore();
	}

	private void tracePolygon(ITurtle painter, int p, double radius) {
		double alpha = 360.0 / p;
		double side = radius * 2 * Math.sin(Math.PI / p);
		// Math.cos(A) == R / L
		// L = R / Math.cos(A)
		if (DEBUG_POLYS) {
			Paint ink = painter.getInk();
			painter.setInk(Color.BLUE);
			painter.draw(radius);
			painter.setInk(ink);
		} else {
			painter.jump(radius);
		}
		double interior = 180 - alpha;
		painter.turn(180 - interior / 2);
		for (int i = 0; i < p; i++) {
			if (RAINBOW_POLYS)
				painter.setInk(Color.hsb(alpha * i, 1.0, 1.0));
			painter.draw(side);
			painter.turn(alpha);
		}
		painter.turn(interior / 2);
		if (false) {
			if (DEBUG_POLYS) {
				Paint ink = painter.getInk();
				painter.setInk(Color.GREEN);
				painter.draw(radius);
				painter.setInk(ink);
			} else {
				painter.jump(radius);
			}
			painter.turn(180);
		}
	}

	private void traceStar(ITurtle painter, int p, double radius) {
		traceStar(painter, p, 360.0 / p, radius);
	}

	private void traceStar(ITurtle painter, int p, double alpha, double radius) {
		double beta = (360.0 + p * alpha) / p;
		if (beta > 179.0) {
			tracePolygon(painter, p, radius);
			return;
		}
		if (radius <= 2) {
			tracePolygon(painter, 3, radius);
			return;
		}
		double side = radius * 2 * Math.sin(Math.PI / p);
		double inSide = (side / 2) / Math.sin(Math.toRadians(beta / 2));
		if (DEBUG_POLYS) {
			Paint ink = painter.getInk();
			painter.setInk(Color.BLUE);
			painter.draw(radius);
			painter.setInk(ink);
		} else {
			painter.jump(radius);
		}
		painter.turn(180 - alpha / 2);
		for (int i = 0; i < p; i++) {
			if (RAINBOW_POLYS)
				painter.setInk(Color.hsb(alpha * i, 1.0, 1.0));
			painter.draw(inSide);
			painter.turn(beta - 180);
			painter.draw(inSide);
			painter.turn(180 - alpha);
		}
		painter.turn(alpha / 2);
		if (false) {
			if (DEBUG_POLYS) {
				Paint ink = painter.getInk();
				painter.setInk(Color.GREEN);
				painter.draw(radius);
				painter.setInk(ink);
			} else {
				painter.jump(radius);
			}
			painter.turn(180);
		}
	}

	public void makeParticleJob() {
		int[] count = new int[1];
		long[] time = new long[1];
		time[0] = System.currentTimeMillis();
		particleTimeline = new Timeline();
		particleTimeline.setCycleCount(Timeline.INDEFINITE);
		KeyFrame kf = new KeyFrame(Duration.millis(100), (ActionEvent event) -> {
			long t = System.currentTimeMillis();
			long timeSinceLast = Math.min(t - time[0], 100);
			int parts = stepParticles(-1, timeSinceLast);
			System.out.println("new frame: " + count[0] + " dt=" + timeSinceLast);
			time[0] = t;
			if (parts == 0) {
				System.out.println("end animation at: " + count[0]);
				particleTimeline.stop();
				particlePos.clear();
				particleVelocity.clear();
				particlePaint.clear();
				particleRotation.clear();
				particleLife.clear();
				particleSize.clear();
			} else {
				// System.out.println("end frame: " + count[0] + " (" + parts + " stars in "
				// + (System.currentTimeMillis() - t) + "ms)");
			}
			count[0]++;
		});
		particleTimeline.setDelay(Duration.millis(0));
		particleTimeline.getKeyFrames().add(kf);
	}

	private int stepParticles(int start, long dT) {
		if (start == -1)
			painter.as(GraphicsContext.class).clearRect(0, 0, 1280, 960);
		int count = 0;
		painter.save();
		double w = getBandWidth();
		for (int i = Math.max(0, start); i < particlePos.size(); i++) {
			if (particlePos.get(i) == null)
				continue;
			count++;
			// painter.setInk(new RadialGradient(0.0, 0, 0.5, 0.5, 0.5, true,
			// CycleMethod.NO_CYCLE, RAINBOW_STOPS));
			Color c = (Color) particlePaint.get(i);
			painter.setInk(c);
			painter.jumpTo(particlePos.get(i));
			painter.turnTo(particleRotation.get(i));
			painter.as(TurtlePainter.class).beginPath();
			traceStar(painter, 5, 36, particleSize.get(i));
			painter.as(TurtlePainter.class).closePath();
			painter.as(TurtlePainter.class).fillPath();
			if (start != -1)
				continue;
			if (particleLife.get(i) < -1000) {
				particlePos.set(i, null);
				// particleVelocity.remove(i);
				// particlePaint.remove(i);
				// particleRotation.remove(i);
				// particleLife.remove(i);
				continue;
			} else if (particleLife.get(i) < 0) {
				particlePaint.set(i, ((Color) particlePaint.get(i)).deriveColor(0.0, 1.0, 1.0, 0.8));
				particlePos.set(i, particlePos.get(i).move(particleVelocity.get(i)));
				particleRotation.set(i, particleRotation.get(i) + 15);
				// particleVelocity.set(i, particleVelocity.get(i).move(0, 0.98));
			} else {
				particlePos.set(i, particlePos.get(i).move(particleVelocity.get(i).scale(0.25)));
				particleRotation.set(i, particleRotation.get(i) + 5);
				// particlePos.set(i, particlePos.get(i).move(new
				// Direction(particleRotation.get(i)), random.nextGaussian()));
			}
			particleLife.set(i, (int) (particleLife.get(i) - dT));
			if (particlePos.get(i).getY() - 300 > painter.getHeight()) {
				particlePos.set(i, null);
				// particleVelocity.remove(i);
				// particlePaint.remove(i);
				// particleRotation.remove(i);
				// particleLife.remove(i);
			}
		}
		painter.restore();
		return count;
	}
}
