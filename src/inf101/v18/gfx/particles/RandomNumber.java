package inf101.v18.gfx.particles;

import java.util.Random;

public class RandomNumber {
	private boolean normal;

	private double deviation;

	private double expected;

	private boolean limit = false;

	static final ThreadLocal<Random> random = new ThreadLocal<Random>() {
		@Override
		protected Random initialValue() {
			return new Random();
		}
	};

	/**
	 * Return generator for normally distributed numbers.
	 *
	 * Generated numbers will use the Gaussian distribution, centered around
	 * <code>mean</code> with a standard deviation of <code>deviation</code>.
	 *
	 * @param mean
	 *            Desired mean value of generated numbers.
	 * @param deviation
	 *            Desired standard deviation
	 * @return A random number generator
	 */
	public static RandomNumber normalAbs(double mean, double deviation) {
		return new RandomNumber(true, mean, deviation);
	}

	/**
	 * Return generator for normally distributed numbers.
	 *
	 * Generated numbers will use the Gaussian distribution, centered around
	 * <code>mean</code> with a standard deviation of
	 * <code>mean*fracDeviation</code>.
	 *
	 * @param mean
	 *            Desired mean value of generated numbers.
	 * @param fracDeviation
	 *            Desired standard deviation, as a fraction of <code>mean</code>
	 * @return A random number generator
	 */
	public static RandomNumber normalRel(double mean, double fracDeviation) {
		return new RandomNumber(true, mean, mean * fracDeviation);
	}

	public static RandomNumber random(double base, double offset, Option[] options) {
		boolean normal = true;
		boolean relative = false;
		for (Option o : options) {
			switch (o) {
			case GAUSSIAN:
				normal = true;
				break;
			case UNIFORM:
				normal = false;
				break;
			case ABSOLUTE:
				relative = false;
				break;
			case RELATIVE:
				relative = true;
				break;
			default:
				break;
			}
		}
		if (relative) {
			return normal ? normalRel(base, offset) : uniformRel(base, offset);
		} else {
			return normal ? normalAbs(base, offset) : uniformAbs(base, offset);
		}

	}

	/**
	 * Return generator for uniformly distributed numbers.
	 *
	 * Generated numbers will be uniformly distributed between <code>base</code>
	 * (inclusive) and <code>base+deviation</code> (exclusive).
	 *
	 * @param base
	 *            The desired minimum value.
	 * @param deviation
	 *            The random component
	 * @return A random number generator
	 */
	public static RandomNumber uniformAbs(double expected, double deviation) {
		return new RandomNumber(false, expected, deviation);
	}

	/**
	 * Return generator for uniformly distributed numbers.
	 *
	 * Generated numbers will be uniformly distributed between <code>base</code>
	 * (inclusive) and <code>base+(base*fracDeviation)</code> (exclusive).
	 *
	 * @param base
	 *            The desired minimum value.
	 * @param fracDeviation
	 *            The random component, expressed as a fraction of
	 *            <code>base</code>
	 * @return A random number generator
	 */
	public static RandomNumber uniformRel(double base, double fracDeviation) {
		return new RandomNumber(false, base, base * fracDeviation);
	}

	private RandomNumber(boolean normal, double expected, double deviation) {
		this.normal = normal;
		this.deviation = deviation;
		this.expected = expected;
	}

	public double next() {
		double dev;
		if (normal) {
			dev = random.get().nextGaussian() * deviation;
		} else {
			dev = random.get().nextDouble() * deviation;
		}
		if (limit) {
			if (dev < 0) {
				dev = Math.max(dev, -2 * deviation);
			} else {
				dev = Math.min(dev, 2 * deviation);
			}
		}
		return expected + dev;
	}

	public enum Option {
		/**
		 * Gaussian (normal) distribution.
		 *
		 * This is the default.
		 */
		GAUSSIAN,
		/** Uniform distribution */
		UNIFORM,
		/** Standard deviation is a fraction of the base */
		RELATIVE,
		/**
		 * Standard deviation is an absolute value.
		 *
		 * This is the default.
		 */
		ABSOLUTE
	}
}
