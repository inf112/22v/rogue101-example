package inf101.v18.gfx.particles;

public enum ParticleProperty {
	X, Y, SIZE, DX, DY, OPACITY, LIFE, TIMESTAMP
}