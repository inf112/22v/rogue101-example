package inf101.v18.gfx.particles;


import inf101.v18.gfx.particles.ParticleCanvas;
import inf101.v18.gfx.particles.systems.Explosion;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class ParticleDemo extends Application {
	private ParticleCanvas canvas;
	// private static final long serialVersionUID = -5830018712727696869L;

	public static void main(String[] args) {
		launch(args);
		// new GUIFrame(startGame());
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		double width = 1024;
		double height = 768;
		Group root = new Group();
		Scene scene = new Scene(root, width, height, Color.BLACK);
		primaryStage.setScene(scene);

		canvas = new Explosion(width, height);
		canvas.widthProperty().bind(scene.widthProperty());
		canvas.heightProperty().bind(scene.heightProperty());
		canvas.start();

		root.getChildren().add(canvas);

//		primaryStage.setFullScreen(true);
		primaryStage.show();
	}

}