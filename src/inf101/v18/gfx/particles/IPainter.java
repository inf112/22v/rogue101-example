package inf101.v18.gfx.particles;

import javafx.scene.paint.Paint;

public interface IPainter {

	Paint getPaint(double time);
}
