package inf101.v18.gfx.particles;

import java.util.Random;

import javafx.scene.paint.Color;

public class ParticleGenerator {
	private boolean relativeX = false;
	private boolean relativeY = false;
	private RandomNumber initialX = RandomNumber.uniformAbs(0.0, 1.0);
	private RandomNumber initialY = RandomNumber.uniformAbs(0.0, 1.0);
	private RandomNumber initialDx = RandomNumber.normalAbs(0.0, 1.0);
	private RandomNumber initialDy = RandomNumber.normalAbs(0.0, 1.0);
	private RandomNumber direction = null;
	private RandomNumber speed = null;

	private RandomNumber initialParticleSize = RandomNumber.normalRel(25, 0.05);

	double particlesPerSecond = 500;
	double secondsPerParticle = 1.0 / particlesPerSecond;
	double timeBudget = 0.0;

	private IPainter particlePaint = new ColorPainter(Color.WHITE);
	private double opacity = 0.1;
	private RandomNumber life = RandomNumber.normalAbs(5.0, 1.0);
	private double spawnIntervalStart = 0.0;
	private double spawnIntervalEnd = Double.MAX_VALUE;

	static final ThreadLocal<Random> random = new ThreadLocal<Random>() {
		@Override
		protected Random initialValue() {
			return new Random();
		}
	};

	public ParticleGenerator copy() {
		ParticleGenerator c = new ParticleGenerator();
		c.relativeX = relativeX;
		c.relativeY = relativeY;

		c.initialX = initialX;
		c.initialY = initialY;

		c.initialDx = initialDx;
		c.initialDy = initialDy;

		c.direction = direction;
		c.speed = speed;

		c.initialParticleSize = initialParticleSize;

		c.particlesPerSecond = particlesPerSecond;
		c.secondsPerParticle = secondsPerParticle;

		c.particlePaint = particlePaint;
		c.opacity = opacity;
		c.life = life;

		c.spawnIntervalStart = spawnIntervalStart;
		c.spawnIntervalEnd = spawnIntervalEnd;

		return c;
	}

	public IPainter getParticlePaint() {
		return particlePaint;
	}

	/**
	 * @return Number of particles that will be spawned per second
	 */
	public double getParticlesPerSecond() {
		return particlesPerSecond;
	}

	public double getSpawnIntervalEnd() {
		return spawnIntervalEnd;
	}

	public double getSpawnIntervalStart() {
		return spawnIntervalStart;
	}

	/**
	 * Called for every time step.
	 *
	 * Puts particles into the particle set, depending on the amount of time
	 * that has elapsed since last step.
	 *
	 * @param canvas
	 *            The canvas onto which the particles will be drawn
	 * @param particles
	 *            The particle set
	 * @param elapsed
	 *            The number of seconds since last time handle was called
	 * @param timestamp
	 *            Timestamp of the current frame in seconds
	 */
	public void handle(ParticleCanvas canvas, ParticleSet particles, double elapsed, double timestamp) {
		if (timestamp >= spawnIntervalStart && timestamp < spawnIntervalEnd) {
			timeBudget += elapsed;
			while (timeBudget >= secondsPerParticle) {
				timeBudget -= secondsPerParticle;
				double x;
				if (relativeX) {
					x = canvas.getNominalWidth() * initialX.next();
				} else {
					x = initialX.next();
				}
				double y;
				if (relativeY) {
					y = canvas.getNominalHeight() * initialY.next();
				} else {
					y = initialY.next();
				}
				double size = initialParticleSize.next();
				if (size < 0.0) {
					size = 0.0;
				}
				int j = particles.addParticle(timestamp, x, y, size);
				if (j >= 0) {
					if (speed != null && direction != null) {
						double dir = direction.next();
						double spd = speed.next();
						double dx = Math.cos(Math.toRadians(dir));
						double dy = Math.sin(Math.toRadians(dir));
						particles.setDY(j, dy * spd);
						particles.setDX(j, dx * spd);
					} else {
						particles.setDY(j, initialDy.next());
						particles.setDX(j, initialDx.next());
					}
					particles.setPainter(j, particlePaint);
					particles.set(ParticleProperty.LIFE, j, life.next());
					particles.set(ParticleProperty.OPACITY, j, opacity);
				}
			}
		}
	}

	/**
	 * Set initial direction of new particles.
	 *
	 *
	 * @param angle
	 *            Angle in degrees, where 0 is right and 90 is up.
	 * @param deviation
	 *            Standard deviation
	 * @param options
	 *            Randomization options
	 */
	public void setDirection(double angle, double deviation, RandomNumber.Option... options) {
		this.direction = RandomNumber.random(angle, deviation, options);
	}

	/**
	 * Set initial speed along X-axis.
	 *
	 * Speed is measured in pixels / second.
	 *
	 * @param initialDX
	 *            Delta-X
	 * @param deviation
	 *            Standard deviation (or 0.0 for non-randomized start)
	 */
	public void setDX(double initialDX, double deviation, RandomNumber.Option... options) {
		this.initialDx = RandomNumber.random(initialDX, deviation, options);
		this.direction = null;
		this.speed = null;
	}

	/**
	 * Set initial speed along Y-axis.
	 *
	 * Speed is measured in pixels / second.
	 *
	 * @param initialDX
	 *            Delta-Y
	 * @param deviation
	 *            Standard deviation (or 0.0 for non-randomized start)
	 */
	public void setDY(double initialDY, double deviation, RandomNumber.Option... options) {
		this.initialDy = RandomNumber.random(initialDY, deviation, options);
		this.direction = null;
		this.speed = null;
	}

	/**
	 * Set life time of newly generated particles.
	 *
	 * @param life
	 *            Life in seconds
	 * @param deviation
	 *            Standard deviation (or 0.0 for non-randomized start)
	 */
	public void setLife(double life, double deviation, RandomNumber.Option... options) {
		this.life = RandomNumber.random(life, deviation, options);
	}

	/**
	 * Set base opacity of particles.
	 *
	 * @param opacity
	 *            The opacity, a number between 0.0 (fully transparent) and 1.0
	 *            (fully opaque)
	 */
	public void setOpacity(double opacity) {
		this.opacity = Math.max(0.0, Math.min(1.0, opacity));
	}

	/**
	 * Set color of new particles
	 *
	 * @param color
	 *            The color
	 */
	public void setParticlePaint(Color color) {
		setParticlePaint(new ColorPainter(color));
	}

	/**
	 * Set color of particles over lifetime
	 *
	 * Color at any given time is interpolated between color1 and color2
	 * according to how far along the particle is in its lifetime.
	 *
	 * @param color1
	 *            Color when particle is spawned
	 * @param color2
	 *            Color at end of particle lifetime
	 */
	public void setParticlePaint(Color color1, Color color2) {
		setParticlePaint(new ColorPainter(color1, color2));
	}

	public void setParticlePaint(IPainter particlePaint) {
		this.particlePaint = particlePaint;
	}

	/**
	 * Set expected size of generated particles.
	 *
	 * @param initialParticleSize
	 *            Particle size in pixels
	 * @param initialParticleDeviation
	 *            Standard deviation (or 0.0 for non-randomized start)
	 */
	public void setParticleSize(double initialParticleSize, double initialParticleDeviation,
			RandomNumber.Option... options) {
		this.initialParticleSize = RandomNumber.random(initialParticleSize, initialParticleDeviation, options);
	}

	/**
	 * Set the number of particles to spawn per second.
	 *
	 * @param particlesPerSecond
	 *            Number of particles to generate
	 */
	public void setParticlesPerSecond(double particlesPerSecond) {
		if (particlesPerSecond < 0.0) {
			throw new IllegalArgumentException();
		}
		this.particlesPerSecond = particlesPerSecond;
		this.secondsPerParticle = 1.0 / particlesPerSecond;

	}

	public void setSpawnInterval(double start, double end) {
		spawnIntervalStart = start;
		spawnIntervalEnd = end;
	}

	public void setSpeed(double speed, double deviation, RandomNumber.Option... options) {
		this.speed = RandomNumber.random(speed, deviation, options);
	}

	/**
	 * Set absolute X-coordinate of spawn point.
	 *
	 * @param initialX
	 *            X-coordinate
	 * @param deviation
	 *            Standard deviation (or 0.0 for non-randomized start)
	 */
	public void setXAbsolute(double initialX, double deviation, RandomNumber.Option... options) {
		this.initialX = RandomNumber.random(initialX, deviation, options);
		this.relativeX = false;
	}

	/**
	 * Set relative X-coordinate of spawn point.
	 *
	 * @param initialX
	 *            X-coordinate as a fraction of screen width
	 * @param deviation
	 *            Standard deviation (or 0.0 for non-randomized start)
	 */
	public void setXRelative(double initialX, double deviation, RandomNumber.Option... options) {
		this.initialX = RandomNumber.random(initialX, deviation, options);
		this.relativeX = true;
	}

	/**
	 * Set absolute X-coordinate of spawn point.
	 *
	 * @param initialY
	 *            Y-coordinate
	 * @param deviation
	 *            Standard deviation (or 0.0 for non-randomized start)
	 */
	public void setYAbsolute(double initialY, double deviation, RandomNumber.Option... options) {
		this.initialY = RandomNumber.random(initialY, deviation, options);
		this.relativeY = false;
	}

	/**
	 * Set relative Y-coordinate of spawn point.
	 *
	 * @param initialY
	 *            Y-coordinate as a fraction of screen height
	 * @param deviation
	 *            Standard deviation (or 0.0 for non-randomized start)
	 */
	public void setYRelative(double initialY, double deviation, RandomNumber.Option... options) {
		this.initialY = RandomNumber.random(initialY, deviation, options);
		this.relativeY = true;
	}
}
