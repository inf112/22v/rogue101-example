package inf101.v18.util.vectors;

import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleUnaryOperator;
import java.util.stream.IntStream;

public class VectorUtilParChunked implements IVectorUtil {
	private int nChunks = 8;

	/*
	 * (non-Javadoc)
	 * 
	 * @see inf101.util.IVectorUtil#add(double[], double, int)
	 */
	@Override
	public final void add(double[] vec, double scalar, int count) {
		if (count < 0 || count > vec.length)
			throw new IllegalArgumentException(String.valueOf(count));

		int chunkSize = (count % nChunks == 0) ? count / nChunks : count / (nChunks - 1);

		IntStream.range(0, nChunks).parallel().forEach(c -> {
			final int start = c * chunkSize;
			final int end = Math.min(count, (c + 1) * chunkSize);
			for (int i = start; i < end; i++)
				vec[i] += scalar;
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see inf101.util.IVectorUtil#add(double[], double[], double, int)
	 */
	@Override
	public final void add(double[] vecA, double[] vecB, double bScale, int count) {
		if (count < 0 || count > vecA.length || count > vecB.length)
			throw new IllegalArgumentException(String.valueOf(count));

		int chunkSize = (count % nChunks == 0) ? count / nChunks : count / (nChunks - 1);

		IntStream.range(0, nChunks).parallel().forEach(c -> {
			final int start = c * chunkSize;
			final int end = Math.min(count, (c + 1) * chunkSize);
			for (int i = start; i < end; i++)
				vecA[i] += bScale * vecB[i];
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see inf101.util.IVectorUtil#add(double[], double[], int)
	 */
	@Override
	public final void add(double[] vecA, double[] vecB, int count) {
		if (count < 0 || count > vecA.length || count > vecB.length)
			throw new IllegalArgumentException(String.valueOf(count));

		int chunkSize = (count % nChunks == 0) ? count / nChunks : count / (nChunks - 1);
//		System.out.println(chunkSize);
		IntStream.range(0, nChunks).parallel().forEach(c -> {
			final int start = c * chunkSize;
			final int end = Math.min(count, (c + 1) * chunkSize);
			// System.out.println("Chunk " + c + ", start " + start + ", end " +
			// end);
			for (int i = start; i < end; i++)
				vecA[i] += vecB[i];
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see inf101.util.IVectorUtil#div(double[], double, int)
	 */
	@Override
	public final void div(double[] vec, double divisor, int count) {
		if (count < 0 || count > vec.length)
			throw new IllegalArgumentException(String.valueOf(count));

		int chunkSize = (count % nChunks == 0) ? count / nChunks : count / (nChunks - 1);

		IntStream.range(0, nChunks).parallel().forEach(c -> {
			final int start = c * chunkSize;
			final int end = Math.min(count, (c + 1) * chunkSize);
			for (int i = start; i < end; i++)
				vec[i] /= divisor;
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see inf101.util.IVectorUtil#div(double[], double[], double, int)
	 */
	@Override
	public final void div(double[] vecA, double[] vecB, double bScale, int count) {
		if (count < 0 || count > vecA.length || count > vecB.length)
			throw new IllegalArgumentException(String.valueOf(count));

		int chunkSize = (count % nChunks == 0) ? count / nChunks : count / (nChunks - 1);

		IntStream.range(0, nChunks).parallel().forEach(c -> {
			final int start = c * chunkSize;
			final int end = Math.min(count, (c + 1) * chunkSize);
			for (int i = start; i < end; i++)
				vecA[i] += bScale * vecB[i];
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see inf101.util.IVectorUtil#div(double[], double[], int)
	 */
	@Override
	public final void div(double[] vecA, double[] vecB, int count) {
		if (count < 0 || count > vecA.length || count > vecB.length)
			throw new IllegalArgumentException(String.valueOf(count));

		int chunkSize = (count % nChunks == 0) ? count / nChunks : count / (nChunks - 1);

		IntStream.range(0, nChunks).parallel().forEach(c -> {
			final int start = c * chunkSize;
			final int end = Math.min(count, (c + 1) * chunkSize);
			for (int i = start; i < end; i++)
				vecA[i] /= vecB[i];
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see inf101.util.IVectorUtil#get(double[], int, int)
	 */
	@Override
	public final double get(double[] vec, int index, int count) {
		if (count < 0 || count > vec.length)
			throw new IllegalArgumentException(String.valueOf(count));
		if (index < 0 || index >= count || index >= vec.length)
			throw new IndexOutOfBoundsException(String.valueOf(count));

		return vec[index];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see inf101.util.IVectorUtil#map(java.util.function.DoubleBinaryOperator,
	 * double[], double, int)
	 */
	@Override
	public final void map(DoubleBinaryOperator fun, double[] vec, double scalar, int count) {
		if (count < 0 || count > vec.length)
			throw new IllegalArgumentException(String.valueOf(count));

		int chunkSize = (count % nChunks == 0) ? count / nChunks : count / (nChunks - 1);

		IntStream.range(0, nChunks).parallel().forEach(c -> {
			final int start = c * chunkSize;
			final int end = Math.min(count, (c + 1) * chunkSize);
			for (int i = start; i < end; i++)
				vec[i] = fun.applyAsDouble(vec[i], scalar);
		});

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see inf101.util.IVectorUtil#map(java.util.function.DoubleBinaryOperator,
	 * double[], double[], int)
	 */
	@Override
	public final void map(DoubleBinaryOperator fun, double[] vecA, double[] vecB, int count) {
		if (count < 0 || count > vecA.length || count > vecB.length)
			throw new IllegalArgumentException(String.valueOf(count));

		int chunkSize = (count % nChunks == 0) ? count / nChunks : count / (nChunks - 1);

		IntStream.range(0, nChunks).parallel().forEach(c -> {
			final int start = c * chunkSize;
			final int end = Math.min(count, (c + 1) * chunkSize);
			for (int i = start; i < end; i++)
				vecA[i] = fun.applyAsDouble(vecA[i], vecB[i]);
		});

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see inf101.util.IVectorUtil#map(java.util.function.DoubleUnaryOperator,
	 * double[], int)
	 */
	@Override
	public final void map(DoubleUnaryOperator fun, double[] vec, int count) {
		if (count < 0 || count > vec.length)
			throw new IllegalArgumentException(String.valueOf(count));

		int chunkSize = (count % nChunks == 0) ? count / nChunks : count / (nChunks - 1);

		IntStream.range(0, nChunks).parallel().forEach(c -> {
			final int start = c * chunkSize;
			final int end = Math.min(count, (c + 1) * chunkSize);
			for (int i = start; i < end; i++)
				vec[i] = fun.applyAsDouble(vec[i]);
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see inf101.util.IVectorUtil#mul(double[], double, int)
	 */
	@Override
	public final void mul(double[] vec, double scale, int count) {
		if (count < 0 || count > vec.length)
			throw new IllegalArgumentException(String.valueOf(count));

		int chunkSize = (count % nChunks == 0) ? count / nChunks : count / (nChunks - 1);

		IntStream.range(0, nChunks).parallel().forEach(c -> {
			final int start = c * chunkSize;
			final int end = Math.min(count, (c + 1) * chunkSize);
			for (int i = start; i < end; i++)
				vec[i] *= scale;
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see inf101.util.IVectorUtil#mul(double[], double[], double, int)
	 */
	@Override
	public final void mul(double[] vecA, double[] vecB, double bScale, int count) {
		if (count < 0 || count > vecA.length || count > vecB.length)
			throw new IllegalArgumentException(String.valueOf(count));

		int chunkSize = (count % nChunks == 0) ? count / nChunks : count / (nChunks - 1);

		IntStream.range(0, nChunks).parallel().forEach(c -> {
			final int start = c * chunkSize;
			final int end = Math.min(count, (c + 1) * chunkSize);
			for (int i = start; i < end; i++)
				vecA[i] *= bScale * vecB[i];
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see inf101.util.IVectorUtil#mul(double[], double[], int)
	 */
	@Override
	public final void mul(double[] vecA, double[] vecB, int count) {
		if (count < 0 || count > vecA.length || count > vecB.length)
			throw new IllegalArgumentException(String.valueOf(count));

		int chunkSize = (count % nChunks == 0) ? count / nChunks : count / (nChunks - 1);

		IntStream.range(0, nChunks).parallel().forEach(c -> {
			final int start = c * chunkSize;
			final int end = Math.min(count, (c + 1) * chunkSize);
			for (int i = start; i < end; i++)
				vecA[i] *= vecB[i];
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see inf101.util.IVectorUtil#set(double[], double, int)
	 */
	@Override
	public final void fill(double[] vec, double value, int count) {
		if (count < 0 || count > vec.length)
			throw new IllegalArgumentException(String.valueOf(count));

		int chunkSize = (count % nChunks == 0) ? count / nChunks : count / (nChunks - 1);

		IntStream.range(0, nChunks).parallel().forEach(c -> {
			final int start = c * chunkSize;
			final int end = Math.min(count, (c + 1) * chunkSize);
			for (int i = start; i < end; i++)
				vec[i] = value;
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see inf101.util.IVectorUtil#set(double[], double[], int)
	 */
	@Override
	public final void fill(double[] vecA, double[] vecB, int count) {
		if (count < 0 || count > vecA.length || count > vecB.length)
			throw new IllegalArgumentException(String.valueOf(count));

		int chunkSize = (count % nChunks == 0) ? count / nChunks : count / (nChunks - 1);

		IntStream.range(0, nChunks).parallel().forEach(c -> {
			final int start = c * chunkSize;
			final int end = Math.min(count, (c + 1) * chunkSize);
			for (int i = start; i < end; i++)
				vecA[i] = vecB[i];
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see inf101.util.IVectorUtil#set(double[], int, double, int)
	 */
	@Override
	public final void set(double[] vec, int index, double value, int count) {
		if (count < 0 || count > vec.length)
			throw new IllegalArgumentException(String.valueOf(count));
		if (index < 0 || index >= count || index >= vec.length)
			throw new IndexOutOfBoundsException(String.valueOf(count));

		vec[index] = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see inf101.util.IVectorUtil#sub(double[], double, int)
	 */
	@Override
	public final void sub(double[] vec, double scalar, int count) {
		if (count < 0 || count > vec.length)
			throw new IllegalArgumentException(String.valueOf(count));

		int chunkSize = (count % nChunks == 0) ? count / nChunks : count / (nChunks - 1);

		IntStream.range(0, nChunks).parallel().forEach(c -> {
			final int start = c * chunkSize;
			final int end = Math.min(count, (c + 1) * chunkSize);
			for (int i = start; i < end; i++)
				vec[i] -= scalar;
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see inf101.util.IVectorUtil#sub(double[], double[], double, int)
	 */
	@Override
	public final void sub(double[] vecA, double[] vecB, double bScale, int count) {
		if (count < 0 || count > vecA.length || count > vecB.length)
			throw new IllegalArgumentException(String.valueOf(count));

		int chunkSize = (count % nChunks == 0) ? count / nChunks : count / (nChunks - 1);

		IntStream.range(0, nChunks).parallel().forEach(c -> {
			final int start = c * chunkSize;
			final int end = Math.min(count, (c + 1) * chunkSize);
			for (int i = start; i < end; i++)
				vecA[i] -= bScale * vecB[i];
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see inf101.util.IVectorUtil#sub(double[], double[], int)
	 */
	@Override
	public final void sub(double[] vecA, double[] vecB, int count) {
		if (count < 0 || count > vecA.length || count > vecB.length)
			throw new IllegalArgumentException(String.valueOf(count));

		int chunkSize = (count % nChunks == 0) ? count / nChunks : count / (nChunks - 1);

		IntStream.range(0, nChunks).parallel().forEach(c -> {
			final int start = c * chunkSize;
			final int end = Math.min(count, (c + 1) * chunkSize);
			for (int i = start; i < end; i++)
				vecA[i] -= vecB[i];
		});
	}
}
